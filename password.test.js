const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 character to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 character to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 character to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 26 character to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })

  test('should minimum of 8 characters or maximum of 25 character to be true', () => {
    expect(checkLength('cd@1234')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })

  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })

  test('should has alphabet Z in password', () => {
    expect(checkAlphabet('Z')).toBe(true)
  })

  test('should has alphabet b in password', () => {
    expect(checkAlphabet('book')).toBe(true)
  })

  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has digit 0 in password', () => {
    expect(checkDigit('cd@0')).toBe(true)
  })

  test('should has digit 1 in password', () => {
    expect(checkDigit('cd@1')).toBe(true)
  })

  test('should has digit 9 in password', () => {
    expect(checkDigit('cd@2591')).toBe(true)
  })

  test('should has not digit in password', () => {
    expect(checkDigit('Cd')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol ! in password', () => {
    expect(checkSymbol('11!11')).toBe(true)
  })

  test('should has symbol @ in password', () => {
    expect(checkSymbol('cd@12')).toBe(true)
  })

  test('should has symbol * in password', () => {
    expect(checkSymbol('12*13')).toBe(true)
  })

  test('should has not symbol in password', () => {
    expect(checkSymbol('Books25268')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password cd@1234 to be false', () => {
    expect(checkPassword('cd@1234')).toBe(false)
  })

  test('should password Book#1234 to be true', () => {
    expect(checkPassword('Book#1234')).toBe(true)
  })

  test('should minimum of 8 characters or maximum of 25 character in password to be true', () => {
    expect(checkPassword('cd@1234')).toBe(false)
  })

  test('should has symbol # in password to be true', () => {
    expect(checkPassword('Books5268')).toBe(false)
  })

  test('should has not digit in password', () => {
    expect(checkPassword('Book#john')).toBe(false)
  })

  test('should has alphabet in password', () => {
    expect(checkPassword('!1256')).toBe(false)
  })
})
